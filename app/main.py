import openai
import telebot
import redis
import json
from config import BOT_BOT_TOKEN
from config import BOT_GPT_TOKEN

DEEP_COUNT = 0
ROLE = ''

openai.api_key = BOT_GPT_TOKEN
bot = telebot.TeleBot(BOT_BOT_TOKEN)

#clear webhook telegram
#bot.delete_webhook()

print('Бот запущен!')

#r = redis.StrictRedis(host='127.0.0.1',
#                      port=6379,
#                      charset="utf-8",
#                      decode_responses=True)
r = redis.StrictRedis(host="redis",
                      port=6379,
                      charset="utf-8",
                      decode_responses=True)

#clear redis
#r.flushall() """

messages = []

#Add bot role, if any
if ROLE:
    messages.append({"role": "system", "content": f"{ROLE}"})


@bot.message_handler(content_types=['text'])
def msg(message):
    messages.clear()
    #Context of previous messages
    if DEEP_COUNT > 0 and bool(r.exists(f'{message.chat.id}')):
        last_messages = r.lrange(message.chat.id, 0, DEEP_COUNT)

        if len(last_messages) < DEEP_COUNT:
            count_messages = len(last_messages)
        else:
            count_messages = DEEP_COUNT
        for i in range(count_messages):
            messages.append(json.loads(last_messages[i]))

    messages.append({"role": "user", "content": message.text})
    try:
        send_message = bot.send_message(
            chat_id=message.chat.id,
            text='Обрабатываю запрос, пожалуйста подождите!')

        completion = openai.ChatCompletion.create(model="gpt-3.5-turbo",
                                                  messages=messages)

        responce = completion.choices[0].message["content"]

        bot.edit_message_text(text=responce,
                              chat_id=message.chat.id,
                              message_id=send_message.message_id)

        responce_redis = "{" + f"\"role\":\"assistant\",\"content\":\"{responce}\"" + "}"
        r.lpush(message.chat.id, responce_redis)

        request_redis = "{" + f"\"role\":\"user\",\"content\":\"{message.text}\"" + "}"
        r.lpush(message.chat.id, request_redis)

    except Exception as err:
        bot.send_message(chat_id=message.chat.id,
                         text=f"error request to openai: {err}")


bot.infinity_polling()