import os

from dotenv import load_dotenv

load_dotenv()

BOT_BOT_TOKEN = os.environ.get("BOT_BOT_TOKEN")
BOT_GPT_TOKEN = os.environ.get("BOT_GPT_TOKEN")
